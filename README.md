# Map Plan Viewer

A single-page application to display all the available plans on the map and to create a plan as well.

## Table of Contents
- [Requirements](#requirements)
- [What are we looking for](#what-are-we-looking-for)
- [Setup](#setup)
- [Wireframe](#wireframe)
- [Details](#details)
  - [NavBar](#navbar)
  - [LeftPanel](#leftpanel)
  - [CenterPanel](#centerpanel)
  - [PlanCard](#plancard)
  - [CreatePlan](#createplan)
- [Contact](#contact)

## Requirements
You will need:
- Latest version of <a href="https://nodejs.org/en/" target="_blank">NodeJS</a>.
- A <a href="http://www.github.com" target="_blank">GitHub</a> account.

## What are we looking for
- Understanding of the problem statement.
- Usage of HTML and CSS.
- Manipulating the power of React to your advantage.
- Understanding of life cycle methods.
- File structuring.
- Component creation and amangement.
- Reusability of code.
- Usability of the interface.

## Setup
This application is already bootstrapped using `create-react-app`. To begin working on this hack, the candidate needs to <a href="https://help.github.com/articles/importing-a-repository-with-github-importer/" target="_blank">import this repository to their GitHub account</a> and clone the GitHub repository.

Once the app has been setup, run the following commands:
- `npm install`
- `npm run start`

## Wireframe
![wireframe](https://gitlab.com/samrith.ithaka/ithaka-frontend-hack/raw/master/ReadmeImages/HackWireframe.png)

## Details
This project is split into 5 major components, namely the `NavBar`, `LeftPanel`, `CenterPanel`, `PlanCard` and `CreatePlan`. We expect the candidate to create atleast these 5 components in your React Application.

For the working view of the `LeftPanel` and the `CenterPanel` with expected map interactions, you can visit <a href="http://www.tripalista.com" target="_blank">Tripalista</a>.

The `body` tag should have these specific details:
- Color: #333
- Font: Open Sans (Google Font)
- Font Size: 13px

The application should not break on any laptop/desktop screen resolution. Mobile devices support is not required. You can use any CSS framework (ex. Bootstrap, Materialize, Foundation).

### NavBar
The Navbar should be of the following constrains
- Height: 50px
- Background: #efefef

### LeftPanel
- Width: 1/4th of the screen.
- Background: #eee.

### CenterPanel
The `CenterPanel` has a map which highlights the cities listed on the plan when a plan is clicked.

- Width: 3/4th of the screen.
- Background: #fff.

### PlanCard
- Height: 200px.

The rest of the design can be as you see fit.

### CreatePlan
The complete design of the `CreatePlan` form can be as per you. Think, research and use the best user interface suitable for this problem. We will be gauging your ability to think from the user's perspective in this component.

## Contact
For any details, you can contact either `samrith@ithaka.travel` or `mithilesh@ithaka.travel`.

<p align="center" style="border-top:1px solid #555; font-size:11px;margin:30px 0 0;">
  This frontend hack is maintained by <a href="http://www.ithaka.travel" target="_blank">Ithaka</a>. All rights reserved.
</p>



